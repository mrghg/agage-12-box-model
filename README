AGAGE 12-box model

For running instructions, see the header information in agage_box_model.pro (it's actually in the middle of the file, just above the agage_box_model script, rather than above the two subroutines).  To run the model you'll need IDL, which requires a license from Exelisvis, or GDL which is a free IDL compiler.  See the "Installing_GDL.rtf" file included in this distribution for instructions on setting up GDL.

The model is available for collaborative use.  Please contact Matt Rigby for details on running and using the model and to report errors (matt.rigby@bristol.ac.uk).


1) First make sure the model folder appears in your IDL path **:

These instructions stolen from: http://slugidl.pbworks.com/w/page/28913708/Adding%20Programs%20to%20Your%20IDL%20Path)

The easiest way to do add a folder to the path that is independent of architecture is the following:
a) Make an IDL startup file. If you have already done this then skip to step 2. You can make an IDL startup file by
     a) creating a file called .idlstartup
     b) within IDL type:
     IDL> pref_set, 'IDL_STARTUP', '/path/to/.idlstartup',/commit
         where you replace '/path/to/.idlstartup' with the path to the file you created for step 1a
b) Now edit your .idlstartup file to include the following lines to include a folder which for example is located in ~/example/
           !PATH=!PATH+':'+Expand_Path('~/example/')
If I wanted to include all subdirectories I would add the following line instead (notice the + sign in front):
     !PATH=!PATH+':'+Expand_Path('+~/example/')

Another good guide to setting up your IDL path is from David Fanning: http://www.idlcoyote.com/code_tips/installcoyote.php.

Make sure the agage-box-model appears in your IDL path by typing:

IDL> print, !path


2) Create a file in your agage-box-model directory called 'agage_box_model_path.pro'. The file will then contain only one line:

agage_box_model_path='/path/to/your/agage-box-model/'

Make sure there is a trailing '/'

Or if you're on windows, the line should be:

agage_box_model_path='c:\path\to\your\agage-box-model\'


3) Running AGAGE box from within IDL:

Two year simulation::
IDL> q = replicate(1., 4, 24) ;Two year simulation, 1Gg/yr in all surface boxes
IDL> q[0, *]=10.  ;10Gg/yr emissions in northern box
IDL> ic = replicate(10., 12) ;Initially uniform mole fraction (10 pmol/mol).
IDL> ppt_out = agage_box_model(ic, q, mol_mass=50.)
IDL> print, ppt_out
 
   10.313749       10.060051       10.015412       10.047651
   10.722616       10.217531       10.067380       10.109805
   11.024041       10.418397       10.142118       10.164657
   11.284635       10.609171       10.226286       10.206647...


4) Running using ASCII input/output:

If you're not comfortable using IDL, you can run the model using ASCII input and output that can be edited using other software (e.g. Excel). To run the model using ASCII input and output, there is a wrapper program called 'agage_box_model_ascii.pro' which uses the inputs in the csv files in the folder ascii_input/ to generate output in the folder ascii_output/. You can run the model by either running the IDL program agage_box_model_ascii.pro from within IDL:

IDL> agage_box_model_ascii

Or you can run it from outside IDL using (in Unix, make sure you've cd'd into the agage-box-model directory):

bash$ idl agage_box_model_run

Mole fractions in the surface boxes should then appear in the ascii_output/ folder.



More details and advanced usage are in the program headers

NOTES to help understand the documentation:

- An array is a multi-dimensional variable for storing data. A 2D array is like a matrix.
- IDL uses column-major array indexing meaning that, for example, in a 2D array the first index corresponds to the column and the second to the row.  I.e. where the documentation says '4 x nMonths array', this means that if we were to print the array, the table would have 4 columns and nMonths rows (where nMonths corresponds to the number of simulation months).
