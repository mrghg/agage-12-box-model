; docformat = 'rst'
;
;+
;
; :Purpose:
;   Batch file to run agage_box_model_ascii from unix command line
;   
; :Example::
;   In Bash shell:
;     idl agage_box_model_run
;
;   Or from within IDL, use
;     @agage_box_model_run
;     (note that IDL will close if you run from within IDL)
;
; :History:
;   Written by: Matt Rigby, MIT, 20th June, 2012
;
;-
.compile agage_box_model_ascii
agage_box_model_ascii
exit
