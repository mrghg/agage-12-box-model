function agage_box_model_ascii_read, filename, columns, columns_ignore=columns_ignore

  if keyword_set(columns_ignore) eq 0 then columns_ignore=0

  str=''
  header=''

  lines=file_lines(filename)
  lines-- ;reduce by 1 for header

  output=dblarr(columns, lines)

  openr, fun, filename, /get_lun
  
    readf, fun, header
    
    for li=0, lines-1 do begin
      readf, fun, str
      output_line=double( strSplit(str, ',', /extract) )
      output[*, li]=output_line[columns_ignore:*]
    endfor
    
  close, fun
  free_lun, fun

  return, output

end

; docformat = 'rst'
;
;+
;
; :Purpose:
;   Wrapper for agage_box_model for ASCII input and output 
;
; :Inputs:
;   csv (comma-separated variable, i.e. columns must be comma-delimited) files in ascii_input folder:
;     emissions.csv: emissions into each of the four surface boxes
;       one row per month, each column represents the emissions into each of the four surface boxes (units=Gg/yr).
;     lifetime.csv: lifetime (years) in each of the boxes. Use to specify e.g. stratospheric or oceanic uptake lifetime.
;       one row per season, each column contains the instantaneous non-OH lifetime in each of the 12 boxes.
;       NOTE: this lifetime is included to account for non-OH loses, e.g. oceanic uptake, stratospheric photolysis, etc.
;       (units=years)
;     ic.csv: initial mole fraction in each of the 12 boxes
;       (units=pmol/mol)
;     mol_mass.csv: molecular mass
;       (units=g/mol)
;     oh_rate.csv: Arrhenius rate constants for OH reaction
;   
; :Keywords:
;
; :Outputs:
;   mf.csv file of monthly means in ascii_output/ folder
;
; :Example::
;   Once the csv files have been prepared in ascii_input/ (strictly follow the layout given
;   in the files that are already there.  Rows can be added, buy columns can't), the output file
;   can be generated by running:
;     
;     IDL> agage_box_model_ascii
;   
;   Monthly mean mole fractions should appear in ascii_out/mf.csv.
;   NOTE that mf.csv MUST be present in the output folder before running (even if it's an empty file). 
;   It will be overwritten.
;
; :History:
; 	Written by: Matt Rigby, University of Bristol, Jun 5, 2012
;
;-
pro agage_box_model_ascii

  compile_opt idl2
  on_error, 2

  @agage_box_model_path

  if !version.os_family eq 'Windows' then slash='\' else slash='/'

  ;Get emissions (one line per month, number of lines defines length of simulation)
  q=agage_box_model_ascii_read(strcompress(agage_box_model_path + 'ascii_input' + slash + 'emissions.csv', /remove_all), 4)

  ;Get initial conditions (one for each of 12 boxes)
  ic=agage_box_model_ascii_read(strcompress(agage_box_model_path + 'ascii_input' + slash + 'ic.csv', /remove_all), 12)

  ;Get stratospheric lifetimes (one line per season)
  lifetime=agage_box_model_ascii_read(strcompress(agage_box_model_path + 'ascii_input' + slash + 'lifetime.csv', /remove_all), 12)

  ;Get OH rate constants
  arr_OH=agage_box_model_ascii_read(strcompress(agage_box_model_path + 'ascii_input' + slash + 'oh_rate.csv', /remove_all), 2)

  ;Get molecular mass
  mol_Mass=agage_box_model_ascii_read(strcompress(agage_box_model_path + 'ascii_input' + slash + 'mol_mass.csv', /remove_all), 1)

  ;Check inputs
  nMonths=n_elements(q[0, *])
  nSeasons=n_elements(lifetime[0, *])
  
  if (nMonths mod 12) ne 0 then begin
    message, strcompress('ASCII INPUT ERROR: ' + string(nMonths) + ' months input, but integer number of years required')
  endif
  
  if nMonths ne 3*nSeasons then begin
    message, strcompress('ASCII INPUT ERROR: ' + string(nMonths) + ' months in emissions.csv, so there should be ' + $
      string(nMonths/3) + ' seasons in lifetime.csv (currently ' + string(nSeasons) + ')')
  endif
  
  ;Run box model
  mf=agage_box_model(ic, q, mol_mass=mol_Mass, lifeTime=lifeTime, arr_OH=arr_OH, /ascii_output)

end